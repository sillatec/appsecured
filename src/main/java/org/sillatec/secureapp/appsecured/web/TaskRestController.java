package org.sillatec.secureapp.appsecured.web;

import org.sillatec.secureapp.appsecured.dao.TaskRepository;
import org.sillatec.secureapp.appsecured.entities.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TaskRestController {
    @Autowired
    TaskRepository taskRepository;
    @GetMapping("/tasks")
    public List<Task> listTask(){
       // System.out.println("taskRepository.findAll() : "+taskRepository.findAll());
        return taskRepository.findAll();
    }
    @PostMapping("/tasks")
    public Task save(@RequestBody Task t){
        return taskRepository.save(t);
    }

}
