package org.sillatec.secureapp.appsecured.web;

import org.sillatec.secureapp.appsecured.entities.AppUser;
import org.sillatec.secureapp.appsecured.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountRestController {
    @Autowired
    AccountService accountService;

    @PostMapping("/register")
    public AppUser register(@RequestBody RegisterForm userForm){
        if(!userForm.getPassword().equals(userForm.getRepassword()))
            throw new RuntimeException("You must confirm your password.");

        AppUser user=accountService.findUserByUsername(userForm.getUsername()) ;
        if(user!=null) throw new RuntimeException("This username already exist.");
        AppUser appUser=new AppUser();
        appUser.setUsername(userForm.getUsername());
        appUser.setPassword(userForm.getPassword());
        accountService.saveUser(appUser);
        accountService.addRoleToUser(appUser.getUsername(),"USER");

      return appUser;

    }
}
