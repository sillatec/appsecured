package org.sillatec.secureapp.appsecured.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
   @Autowired
   private BCryptPasswordEncoder bCryptPasswordEncoder;
   @Autowired
   private UserDetailsService  userDetailsService; //****** 3eme Methode Basique*****
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //****** 1ere Methode Basique*****
        /*auth.inMemoryAuthentication()
                .withUser("admin").password("1234").roles("ADMIN","USER")
                .and()
                .withUser("user").password("1234").roles("USER");*/

        //****** 2ere Methode Basique*****
        /*auth.jdbcAuthentication()
            .usersByUsernameQuery("ecrire la requeste pour chercher le user")
            .authoritiesByUsernameQuery("")   ; */

        //****** suite 3eme Methode Basique*****
        auth.userDetailsService(userDetailsService)
        .passwordEncoder(bCryptPasswordEncoder);



    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //http.formLogin().loginPage("/login"); redirection vers notre propre formulaire de login
        //http.formLogin(); diseable form login
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/login/**","/register/**").permitAll();

        http.authorizeRequests().antMatchers(HttpMethod.POST,"/task/**").hasAuthority("ADMIN");

        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(new JWTAuthenticationFilter(authenticationManager()));
        http.addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @SuppressWarnings("deprecation")
    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
}


