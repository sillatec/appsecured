package org.sillatec.secureapp.appsecured;

import org.sillatec.secureapp.appsecured.dao.TaskRepository;
import org.sillatec.secureapp.appsecured.entities.AppRole;
import org.sillatec.secureapp.appsecured.entities.AppUser;
import org.sillatec.secureapp.appsecured.entities.Task;
import org.sillatec.secureapp.appsecured.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class AppsecuredApplication implements CommandLineRunner{

    @Autowired
	TaskRepository taskRepository;
    @Autowired
    AccountService accountService;

	public static void main(String[] args) {
		SpringApplication.run(AppsecuredApplication.class, args);
	}
    @Bean
	public BCryptPasswordEncoder getBCPE(){
		return new BCryptPasswordEncoder();
	}
	@Override
	public void run(String... args) throws Exception {

		accountService.saveUser(new AppUser("admin","1234",null));
		accountService.saveUser(new AppUser("user","1234",null));
		accountService.saveRole(new AppRole(null,"ADMIN"));
		accountService.saveRole(new AppRole(null,"USER"));
		accountService.addRoleToUser("admin","ADMIN");
		accountService.addRoleToUser("admin","USER");
		accountService.addRoleToUser("user","USER");
		Stream.of("T1","T2","T3").forEach((String t) ->{
		 taskRepository.save(new Task(t));
		});
		taskRepository.findAll().forEach(t->{
			System.out.println(t);
		});
	}
}
