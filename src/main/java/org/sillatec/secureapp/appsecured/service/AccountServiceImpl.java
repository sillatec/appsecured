package org.sillatec.secureapp.appsecured.service;

import org.sillatec.secureapp.appsecured.dao.AppRoleRepository;
import org.sillatec.secureapp.appsecured.dao.AppUserRepository;
import org.sillatec.secureapp.appsecured.entities.AppRole;
import org.sillatec.secureapp.appsecured.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppRoleRepository appRoleRepository;

    @Override
    public AppUser saveUser(AppUser user) {
        String hashPW= bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hashPW);
        return appUserRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return appRoleRepository.save(role);
    }

    @Override
    public AppUser findUserByUsername(String userName) {
        return appUserRepository.findByUsername(userName);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        AppUser user=appUserRepository.findByUsername(userName);
        AppRole role=appRoleRepository.findByRolename(roleName);
        user.getRoles().add(role);
    }
}
