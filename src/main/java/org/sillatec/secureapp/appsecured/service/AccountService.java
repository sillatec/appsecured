package org.sillatec.secureapp.appsecured.service;

import org.sillatec.secureapp.appsecured.entities.AppRole;
import org.sillatec.secureapp.appsecured.entities.AppUser;

public interface AccountService {
    public AppUser saveUser(AppUser user);
    public AppRole saveRole(AppRole role);
    public AppUser  findUserByUsername(String userName);
    public void addRoleToUser(String userName, String roleName);

}
