package org.sillatec.secureapp.appsecured.service;

import org.sillatec.secureapp.appsecured.entities.AppRole;
import org.sillatec.secureapp.appsecured.entities.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl  implements UserDetailsService {
    @Autowired
    private AccountService accountService;
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
      AppUser user= accountService.findUserByUsername(userName);
      if(user==null) throw new UsernameNotFoundException(userName);
      Collection<GrantedAuthority>  authorities = new ArrayList<>();
      user.getRoles().forEach(r->{
          authorities.add(new SimpleGrantedAuthority(r.getRolename()));
      });
        return new User(user.getUsername(), user.getPassword(), authorities);
        //return new User("hh","",null);
    }
}
