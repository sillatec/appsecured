package org.sillatec.secureapp.appsecured.dao;

import org.sillatec.secureapp.appsecured.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {

    public AppRole findByRolename(String roleName);
}
